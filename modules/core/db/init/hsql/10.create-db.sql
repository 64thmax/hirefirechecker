-- begin HIREFIRECHECKER_ACCESS_GROUP
create table HIREFIRECHECKER_ACCESS_GROUP (
    ID varchar(36) not null,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    NAME varchar(255),
    DESCRIPTION longvarchar,
    --
    primary key (ID)
)^
-- end HIREFIRECHECKER_ACCESS_GROUP
-- begin HIREFIRECHECKER_TELEGRAM_CHANNEL
create table HIREFIRECHECKER_TELEGRAM_CHANNEL (
    ID varchar(36) not null,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    NAME varchar(255),
    TELEGRAM_IDENTIFIER varchar(255),
    DESCRIPTION varchar(255),
    --
    primary key (ID)
)^
-- end HIREFIRECHECKER_TELEGRAM_CHANNEL
-- begin HIREFIRECHECKER_ACCESS_GROUP_TELEGRAM_CHANNEL_LINK
create table HIREFIRECHECKER_ACCESS_GROUP_TELEGRAM_CHANNEL_LINK (
    TELEGRAM_CHANNEL_ID varchar(36) not null,
    ACCESS_GROUP_ID varchar(36) not null,
    primary key (TELEGRAM_CHANNEL_ID, ACCESS_GROUP_ID)
)^
-- end HIREFIRECHECKER_ACCESS_GROUP_TELEGRAM_CHANNEL_LINK
-- begin SEC_USER
alter table SEC_USER add column IS_DELETED boolean ^
alter table SEC_USER add column TELEGRAM_ACCOUNT varchar(255) ^
alter table SEC_USER add column SKYPE_ACCOUNT varchar(255) ^
alter table SEC_USER add column DTYPE varchar(100) ^
update SEC_USER set DTYPE = 'hirefirechecker$ExtUser' where DTYPE is null ^
-- end SEC_USER
-- begin HIREFIRECHECKER_EXT_USER_ACCESS_GROUP_LINK
create table HIREFIRECHECKER_EXT_USER_ACCESS_GROUP_LINK (
    EXT_USER_ID varchar(36) not null,
    ACCESS_GROUP_ID varchar(36) not null,
    primary key (EXT_USER_ID, ACCESS_GROUP_ID)
)^
-- end HIREFIRECHECKER_EXT_USER_ACCESS_GROUP_LINK
