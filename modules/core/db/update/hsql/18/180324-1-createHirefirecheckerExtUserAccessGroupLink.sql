create table HIREFIRECHECKER_EXT_USER_ACCESS_GROUP_LINK (
    EXT_USER_ID varchar(36) not null,
    ACCESS_GROUP_ID varchar(36) not null,
    primary key (EXT_USER_ID, ACCESS_GROUP_ID)
);
