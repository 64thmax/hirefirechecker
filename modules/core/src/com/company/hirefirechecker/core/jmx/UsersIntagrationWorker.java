package com.company.hirefirechecker.core.jmx;

import com.company.hirefirechecker.constants.IntegrationConstants;
import com.company.hirefirechecker.service.DeltaCalculationService;
import com.company.hirefirechecker.service.UsersActualizationService;
import com.haulmont.cuba.security.app.Authenticated;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.List;
import java.util.Map;

@Component(UsersIntagrationWorkerMBean.NAME)
public class UsersIntagrationWorker implements UsersIntagrationWorkerMBean {

    private static final String RESULT = "Users list successfully refreshed";

    @Inject
    private DeltaCalculationService deltaCalculationService;

    @Inject
    private UsersActualizationService usersActualizationService;

    @Authenticated
    @Override
    public String loadUsersList() {
        Map deltaMap = deltaCalculationService.calculateDelta(null);

        usersActualizationService.addUsers((List) deltaMap.get(IntegrationConstants.DELTA_CALCULATION_ADD));
        usersActualizationService.removeUsers((List) deltaMap.get(IntegrationConstants.DELTA_CALCULATION_REMOVE));

        return RESULT;
    }
}
