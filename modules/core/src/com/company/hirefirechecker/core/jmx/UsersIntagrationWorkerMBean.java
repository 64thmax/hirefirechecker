package com.company.hirefirechecker.core.jmx;

import org.springframework.jmx.export.annotation.ManagedOperation;
import org.springframework.jmx.export.annotation.ManagedResource;


@ManagedResource(description = "Service for refreshing users list from LDAP")
public interface UsersIntagrationWorkerMBean {
    String NAME = "hirefirechecher_UsersIntagrationWorkerMBean";

    @ManagedOperation(description = "Refresh users list now")
    String loadUsersList();
}
