package com.company.hirefirechecker.service;

import com.company.hirefirechecker.constants.IntegrationConstants;
import com.company.hirefirechecker.entity.ExtUser;
import com.haulmont.cuba.core.EntityManager;
import com.haulmont.cuba.core.Persistence;
import com.haulmont.cuba.core.Query;
import com.haulmont.cuba.core.Transaction;
import com.haulmont.cuba.core.global.Metadata;
import com.haulmont.cuba.security.entity.Group;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component(DeltaCalculationService.NAME)
public class DeltaCalculationServiceBean implements DeltaCalculationService {

    @Inject
    private Persistence persistence;

    @Inject
    private UsersActualizationService usersActualizationService;

    @Inject
    private Metadata metadata;

    @Override
    public Map<String, List> calculateDelta(List usersList) {
        Map<String, List> deltaMap = new HashMap<>();

        List<ExtUser> addList = new ArrayList<>();
        ExtUser addUser = (ExtUser) metadata.create("hirefirechecker$ExtUser");
        addUser.setLogin(IntegrationConstants.ADD_LOGIN);
        addUser.setTelegramAccount(IntegrationConstants.ADD_TELEGRAM);
        addUser.setGroup(getGroupForUser());
        addList.add(addUser);
        deltaMap.put(IntegrationConstants.DELTA_CALCULATION_ADD, addList);

        List<ExtUser> removeList = new ArrayList<>();
        ExtUser removeUser = (ExtUser) metadata.create("hirefirechecker$ExtUser");
        removeUser.setLogin(IntegrationConstants.REMOVE_LOGIN);
        removeUser.setTelegramAccount(IntegrationConstants.REMOVE_TELEGRAM);
        removeList.add(removeUser);
        deltaMap.put(IntegrationConstants.DELTA_CALCULATION_REMOVE, removeList);

        return deltaMap;
    }

    private Group getGroupForUser() {
        try (Transaction tx = persistence.createTransaction()) {
            EntityManager em = persistence.getEntityManager();

            Query query = em.createQuery(
                        "select g from sec$Group g where g.name = :name");
            query.setParameter("name", IntegrationConstants.ADD_GROUP_NAME);
            Group resultGroup = (Group) query.getFirstResult();

            tx.commit();

            return resultGroup;
        }
    }
}
