package com.company.hirefirechecker.service;

import com.company.hirefirechecker.entity.ExtUser;
import com.haulmont.cuba.core.app.EmailSenderAPI;
import com.haulmont.cuba.core.entity.SendingMessage;
import com.haulmont.cuba.core.global.AppBeans;
import com.haulmont.cuba.core.global.DataManager;
import com.haulmont.cuba.core.global.LoadContext;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import javax.mail.MessagingException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service(SendInviteMessageService.NAME)
public class SendInviteMessageServiceBean implements SendInviteMessageService {

    private static final String USER_BY_ID_QUERY = "";
    private static final String ALL_USERS_QUERY = "select u from sec$User u";

    EmailSenderAPI emailSenderAPI;
    DataManager dataManager;

    private String inviteLink = "https://t.me/joinchat/BrynKUyHxEYHUQJ-b2UVGA";

    public void sendInvite() {
        emailSenderAPI = AppBeans.get(EmailSenderAPI.class);
        dataManager = AppBeans.get(DataManager.class);


        List<SendingMessage> messages = getInviteMessageForUser(getAllUsers());

        if (!messages.isEmpty()) {
            sendInviteEmails(messages);
        }

    }

    private void sendInviteEmails(List<SendingMessage> messages) {
        for (SendingMessage message : messages) {
            try {
                emailSenderAPI.sendEmail(message);
            } catch (MessagingException e) {
                e.printStackTrace();
            }
        }
    }

    private List<SendingMessage> getInviteMessageForUser(List<ExtUser> users) {
        List<SendingMessage> inviteMessages = new ArrayList<>();

        for (ExtUser user : users) {
            SendingMessage message = fillMessageForUser(user);
            if (message != null) {
                inviteMessages.add(message);
            }
        }

        return inviteMessages;
    }

    private SendingMessage fillMessageForUser(ExtUser user) {
        if (user.getEmail() == null) {
            return null;
        }
        SendingMessage sendingMessage = new SendingMessage();

        sendingMessage.setAddress(user.getEmail());
        sendingMessage.setContentText("test text, invite link: " + inviteLink);

        return sendingMessage;
    }

    private List<ExtUser> getUsersByIds(List<UUID> userIds) {
        LoadContext<ExtUser> loadContext = LoadContext.create(ExtUser.class)
                .setQuery(LoadContext.createQuery(USER_BY_ID_QUERY)
                        .setParameter("userIds", userIds))
                .setView("_local");
        return dataManager.loadList(loadContext);
    }

    private List<ExtUser> getAllUsers() {
        LoadContext<ExtUser> loadContext = LoadContext.create(ExtUser.class)
                .setQuery(LoadContext.createQuery(ALL_USERS_QUERY))
                .setView("_local");
        return dataManager.loadList(loadContext);
    }


}