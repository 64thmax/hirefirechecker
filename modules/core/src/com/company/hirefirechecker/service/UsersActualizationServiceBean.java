package com.company.hirefirechecker.service;

import com.company.hirefirechecker.entity.ExtUser;
import com.haulmont.cuba.core.EntityManager;
import com.haulmont.cuba.core.Persistence;
import com.haulmont.cuba.core.Query;
import com.haulmont.cuba.core.Transaction;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.List;

@Component(UsersActualizationService.NAME)
public class UsersActualizationServiceBean implements UsersActualizationService {

    @Inject
    private Persistence persistence;

    @Override
    public void addUsers(List usersList) {
        try (Transaction tx = persistence.createTransaction()) {
            EntityManager em = persistence.getEntityManager();

            for (Object userObj : usersList) {
                ExtUser user = (ExtUser) userObj;
                em.persist(user);
            }

            tx.commit();
        }
    }

    @Override
    public void removeUsers(List usersList) {
        try (Transaction tx = persistence.createTransaction()) {
            EntityManager em = persistence.getEntityManager();

            for (Object userObj : usersList) {
                ExtUser user = (ExtUser) userObj;

                Query query = em.createQuery(
                        "select u from hirefirechecker$ExtUser u where u.login = :login and u.telegramAccount = :telegramAccount");
                query.setParameter("login", user.getLogin());
                query.setParameter("telegramAccount", user.getTelegramAccount());
                ExtUser resultUser = (ExtUser) query.getFirstResult();

                if (resultUser != null) {
                    em.remove(resultUser);
                }
            }

            tx.commit();
        }
    }
}
