package com.company.hirefirechecker.constants;

public interface IntegrationConstants {

    String DELTA_CALCULATION_ADD = "add";
    String DELTA_CALCULATION_REMOVE = "remove";

    String ADD_LOGIN = "111";
    String ADD_TELEGRAM = "@111";
    String ADD_GROUP_NAME = "Company";
    String REMOVE_LOGIN = "222";
    String REMOVE_TELEGRAM = "@222";
}
