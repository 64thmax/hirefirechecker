package com.company.hirefirechecker.entity;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Column;
import javax.persistence.Lob;
import com.haulmont.cuba.core.entity.StandardEntity;
import com.haulmont.chile.core.annotations.NamePattern;
import java.util.Set;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import java.util.List;

@NamePattern("%s|name")
@Table(name = "HIREFIRECHECKER_ACCESS_GROUP")
@Entity(name = "hirefirechecker$AccessGroup")
public class AccessGroup extends StandardEntity {
    private static final long serialVersionUID = -3735509317309035665L;

    @Column(name = "NAME")
    protected String name;

    @JoinTable(name = "HIREFIRECHECKER_ACCESS_GROUP_TELEGRAM_CHANNEL_LINK",
        joinColumns = @JoinColumn(name = "ACCESS_GROUP_ID"),
        inverseJoinColumns = @JoinColumn(name = "TELEGRAM_CHANNEL_ID"))
    @ManyToMany
    protected Set<TelegramChannel> telegramChannels;

    @Lob
    @Column(name = "DESCRIPTION")
    protected String description;


    @JoinTable(name = "HIREFIRECHECKER_EXT_USER_ACCESS_GROUP_LINK",
        joinColumns = @JoinColumn(name = "ACCESS_GROUP_ID"),
        inverseJoinColumns = @JoinColumn(name = "EXT_USER_ID"))
    @ManyToMany
    protected List<ExtUser> users;

    public void setUsers(List<ExtUser> users) {
        this.users = users;
    }

    public List<ExtUser> getUsers() {
        return users;
    }


    public void setTelegramChannels(Set<TelegramChannel> telegramChannels) {
        this.telegramChannels = telegramChannels;
    }

    public Set<TelegramChannel> getTelegramChannels() {
        return telegramChannels;
    }


    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }


}