package com.company.hirefirechecker.entity;

import javax.persistence.Entity;
import com.haulmont.cuba.core.entity.annotation.Extends;
import java.util.List;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import com.haulmont.cuba.security.entity.User;
import javax.persistence.Column;

@Extends(User.class)
@Entity(name = "hirefirechecker$ExtUser")
public class ExtUser extends User {
    private static final long serialVersionUID = 2125008662284663135L;

    @JoinTable(name = "HIREFIRECHECKER_EXT_USER_ACCESS_GROUP_LINK",
        joinColumns = @JoinColumn(name = "EXT_USER_ID"),
        inverseJoinColumns = @JoinColumn(name = "ACCESS_GROUP_ID"))
    @ManyToMany
    protected List<AccessGroup> accessGroups;

    @Column(name = "IS_DELETED")
    protected Boolean isDeleted;

    @Column(name = "TELEGRAM_ACCOUNT")
    protected String telegramAccount;

    @Column(name = "SKYPE_ACCOUNT")
    protected String skypeAccount;

    public void setIsDeleted(Boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    public Boolean getIsDeleted() {
        return isDeleted;
    }


    public void setTelegramAccount(String telegramAccount) {
        this.telegramAccount = telegramAccount;
    }

    public String getTelegramAccount() {
        return telegramAccount;
    }

    public void setSkypeAccount(String skypeAccount) {
        this.skypeAccount = skypeAccount;
    }

    public String getSkypeAccount() {
        return skypeAccount;
    }


    public void setAccessGroups(List<AccessGroup> accessGroups) {
        this.accessGroups = accessGroups;
    }

    public List<AccessGroup> getAccessGroups() {
        return accessGroups;
    }


}