package com.company.hirefirechecker.entity;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Column;
import com.haulmont.cuba.core.entity.StandardEntity;
import com.haulmont.chile.core.annotations.NamePattern;
import java.util.List;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

@NamePattern("%s|name")
@Table(name = "HIREFIRECHECKER_TELEGRAM_CHANNEL")
@Entity(name = "hirefirechecker$TelegramChannel")
public class TelegramChannel extends StandardEntity {
    private static final long serialVersionUID = 8165419511488276259L;

    @Column(name = "NAME")
    protected String name;

    @Column(name = "TELEGRAM_IDENTIFIER")
    protected String telegramIdentifier;

    @Column(name = "DESCRIPTION")
    protected String description;

    @JoinTable(name = "HIREFIRECHECKER_ACCESS_GROUP_TELEGRAM_CHANNEL_LINK",
        joinColumns = @JoinColumn(name = "TELEGRAM_CHANNEL_ID"),
        inverseJoinColumns = @JoinColumn(name = "ACCESS_GROUP_ID"))
    @ManyToMany
    protected List<AccessGroup> accessGroups;

    public void setAccessGroups(List<AccessGroup> accessGroups) {
        this.accessGroups = accessGroups;
    }

    public List<AccessGroup> getAccessGroups() {
        return accessGroups;
    }


    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setTelegramIdentifier(String telegramIdentifier) {
        this.telegramIdentifier = telegramIdentifier;
    }

    public String getTelegramIdentifier() {
        return telegramIdentifier;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }


}