package com.company.hirefirechecker.service;

import java.util.List;
import java.util.Map;

public interface DeltaCalculationService {
    String NAME = "hirefirechecher_DeltaCalculationService";

    Map<String, List> calculateDelta(List usersList);
}
