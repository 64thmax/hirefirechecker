package com.company.hirefirechecker.service;


import java.util.List;
import java.util.UUID;

public interface SendInviteMessageService {
    String NAME = "hirefirechecker_SendInviteMessageService";

    void sendInvite();
}