package com.company.hirefirechecker.service;

import java.util.List;

public interface UsersActualizationService {
    String NAME = "hirefirechecher_UsersActualizationService";

    void addUsers(List usersList);

    void removeUsers(List usersList);
}
