package com.company.hirefirechecker.web.accessgroup;

import com.company.hirefirechecker.entity.AccessGroup;
import com.haulmont.cuba.core.entity.Entity;
import com.haulmont.cuba.gui.components.AbstractLookup;
import com.haulmont.cuba.gui.components.Component;
import com.haulmont.cuba.gui.components.Label;
import com.haulmont.cuba.gui.components.Table;
import com.haulmont.cuba.security.entity.User;
import com.haulmont.cuba.web.gui.components.WebLabel;
import org.apache.commons.lang.text.StrBuilder;

import javax.inject.Inject;
import java.util.Map;

public class AccessGroupBrowse extends AbstractLookup {

    private static final String USERS_COLUMN_ID = "usersColumn";
    private static final String USERS_COLUMN_CAPTION = "usersColumnCaption";

    @Inject
    Table accessGroupsTable;

    @Override
    public void init(Map<String, Object> params) {
        super.init(params);

        generateUsersColumn();
    }

    private void generateUsersColumn() {
        accessGroupsTable.addGeneratedColumn(USERS_COLUMN_ID, new Table.ColumnGenerator() {
            @Override
            public Component generateCell(Entity entity) {
                Label label = new WebLabel();
                label.setValue(generateUsersLabel((AccessGroup)entity));
                return label;
            }
        });

        accessGroupsTable.getColumn(USERS_COLUMN_ID).setCaption(getMessage(USERS_COLUMN_CAPTION));
        accessGroupsTable.getColumn(USERS_COLUMN_ID).setMaxTextLength(150);
    }

    private String generateUsersLabel(AccessGroup entity) {
        StringBuilder strBuilder = new StringBuilder();
        if (entity.getUsers() != null && !entity.getUsers().isEmpty()) {
            for (User user : entity.getUsers()) {
                strBuilder.append(user.getName()).append(",\n");
            }
            String label = strBuilder.toString();
            return label.substring(0, label.length()-2);
        }
        return "";
    }
}