package com.company.hirefirechecker.web.accessgroup;

import com.company.hirefirechecker.entity.ExtUser;
import com.haulmont.cuba.core.entity.Entity;
import com.haulmont.cuba.gui.components.*;
import com.company.hirefirechecker.entity.AccessGroup;
import com.haulmont.cuba.gui.data.CollectionDatasource;

import javax.inject.Inject;
import java.util.Map;

public class AccessGroupEdit extends AbstractEditor<AccessGroup> {

    @Inject
    LookupPickerField lookupUser;
    @Inject
    CollectionDatasource usersDs;

    @Override
    public void init(Map<String, Object> params) {
        super.init(params);

        initListenerForLookupUser();
    }

    private void initListenerForLookupUser() {
        lookupUser.addValueChangeListener(new ValueChangeListener() {
            @Override
            public void valueChanged(ValueChangeEvent e) {
                if (e.getValue() != null) {
                    if (!usersDs.getItems().contains(e.getValue())) {
                        usersDs.addItem((ExtUser) e.getValue());
                    }
                }
            }
        });
    }
}